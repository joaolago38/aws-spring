package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InventarioDto {


    private Integer inventoryId;
    private Integer filmId;
    private Integer storeId;
    private String lastUpdate;


}
