package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoriaDto {

    private Integer categoryId;
    private String lastUpdate;

}
