package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilmeActorDto {

    private Integer actorId;
    private Integer filmId;
    private String lastUpdate;
}
