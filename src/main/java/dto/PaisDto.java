package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaisDto {


    private Integer  countryId;
    private String country;
    private String lastUpdate;
}
