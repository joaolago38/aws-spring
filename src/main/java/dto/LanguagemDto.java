package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguagemDto {


    private  Integer languageId;
    private String name;
    private String lastUpdate;
}
