package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PagamentoDto {


    private Integer paymentId;
    private Long customerId;
    private Long staffId;
    private Long rentalId;
    private Long amount;

    private String paymentDate;
}
