package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PessoaDto {

    private Integer staffId;
    private String firstName;
    private String lastName;
    private Long addressId;
    private String email;
    private Integer storeId;
    private Boolean active;
    private String username;
    private String password;
    private String lastUpdate;
    private String picture; 
}
