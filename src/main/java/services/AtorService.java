package services;


import model.AtorModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import repositores.AtorRespository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AtorService {
    final AtorRespository atorRespository;

    public AtorService(AtorRespository atorRespository) {
        this.atorRespository = atorRespository;
    }

    @Transactional
    public AtorModel save(AtorModel atorModel) {
        return atorRespository.save(atorModel);
    }

    public Page<AtorModel> findAll(Pageable pageable) {
        return atorRespository.findAll(pageable);
    }

    public Optional<AtorModel> findById(Integer actorId) {
        return atorRespository.findById(actorId);
    }
    @Transactional
    public void delete(AtorModel atorModel) {
        atorRespository.delete(atorModel);
    }
}
