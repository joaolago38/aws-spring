package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;


@Entity
@Table(name = "film_actor")
@Getter
@Setter
@Builder
public class FilmeAtorModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer actorId;
    @Column(name = "film_id")
    private Integer filmId;
    @Column(name = "last_update")
    private String lastUpdate;

    public FilmeAtorModel() {

    }

    public FilmeAtorModel(Integer actorId, Integer filmId, String lastUpdate) {
        this.actorId = actorId;
        this.filmId = filmId;
        this.lastUpdate = lastUpdate;
    }
}
