package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;


@Entity
@Table(name = "language")
@Getter
@Setter
@Builder
public class LinguagemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "language_language_id_seq")
    @SequenceGenerator(name = "language_language_id_seq", sequenceName = "language_language_id_seq", allocationSize = 1)
    private Integer languageId;
    @Column(name = "name")
    private String name;
    @Column(name = "last_update")
    private String lastUpdate;

    public LinguagemModel() {

    }

    public LinguagemModel(Integer languageId, String name, String lastUpdate) {
        this.languageId = languageId;
        this.name = name;
        this.lastUpdate = lastUpdate;
    }
}
