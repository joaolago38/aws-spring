package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;


@Entity
@Table(name = "film_category")
@Getter
@Setter
@Builder
public class FilmeCategoriaModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer filmId;
    @Column(name = "category_id")
    private Integer categoryId;
    @Column(name = "last_update")
    private String lastUpdate;

    public FilmeCategoriaModel() {

    }

    public FilmeCategoriaModel(Integer filmId, Integer categoryId, String lastUpdate) {
        this.filmId = filmId;
        this.categoryId = categoryId;
        this.lastUpdate = lastUpdate;
    }
}
