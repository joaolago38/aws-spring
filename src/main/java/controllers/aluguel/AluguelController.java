package controllers.aluguel;


import dto.AluguelDto;
import model.AluguelModel;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import services.AluguelService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;



@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/aluguel")
public class AluguelController {

   final AluguelService aluguelService;

    public AluguelController(AluguelService aluguelService) {
        this.aluguelService = aluguelService;
    }

    @RequestMapping(value = "/aluguelAll", method = RequestMethod.GET)
    public ResponseEntity<List<AluguelModel>> listAllUsers() {
        List<AluguelModel> aluguelModel = aluguelService.findAll();
        if (aluguelModel.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<AluguelModel>>(aluguelModel, HttpStatus.OK);
    }

    @GetMapping("/{rental_id}")
    public ResponseEntity<Object> getBuscaPorID(@PathVariable(value = "rental_id") Integer rentalId) {
        Optional<AluguelModel> aluguelModelOptional = aluguelService.findById(rentalId);
        if (!aluguelModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Aluguel nao encontrado.");
        }
        return ResponseEntity.status(HttpStatus.OK).body(aluguelModelOptional.get());
    }

    @DeleteMapping("/{rentalId}")
    public ResponseEntity<Object> deleteAluguelModel(@PathVariable(value = "rentalId") Integer rentalId){
        Optional<AluguelModel> aluguelModelOptional = aluguelService.findById(rentalId);
        if (!aluguelModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Alugeul nao econtrado.");
        }
        aluguelService.delete(aluguelModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Aluguel deletado com sucesso");
    }
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Object> saveAluguel(@RequestBody @Valid AluguelDto aluguelDto){
        var valorLocacao = aluguelService.findById(aluguelDto.getRentalId());
        if(valorLocacao.isPresent()){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Conflict: Aluguel ja cadastrado!");
        }
        var aluguelModel = new AluguelModel();
        BeanUtils.copyProperties(aluguelDto, aluguelModel);
        return ResponseEntity.status(HttpStatus.CREATED).body(aluguelService.save(aluguelModel));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateAluguel(@PathVariable(value = "rentalId")  Integer rentalId,
                                                @RequestBody @Valid AluguelDto aluguelDto){
        Optional<AluguelModel> aluguelModelOptional = aluguelService.findById(rentalId);
        if (!aluguelModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Parking Spot not found.");
        }
        var aluguelModel = new AluguelModel();
        BeanUtils.copyProperties(aluguelDto, aluguelModel);
        aluguelModel.setRentalId(aluguelModelOptional.get().getRentalId());
        aluguelModel.setLastUpdate(aluguelModelOptional.get().getLastUpdate());
        aluguelModel.setCustomerId(aluguelModelOptional.get().getCustomerId());
        return ResponseEntity.status(HttpStatus.CREATED).body(aluguelService.save(aluguelModel));
    }
}
