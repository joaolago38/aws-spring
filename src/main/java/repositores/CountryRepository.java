package repositores;


import model.PaisModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<PaisModel, Integer  > {

}
