package repositores;


import model.InventarioModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventarioRepository extends JpaRepository<InventarioModel, Integer  > {

}
