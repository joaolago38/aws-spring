package repositores;


import model.ClienteModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<ClienteModel, Integer  > {

}
