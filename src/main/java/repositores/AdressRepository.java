package repositores;


import model.EnderecoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdressRepository extends JpaRepository<EnderecoModel, Integer > {

}
