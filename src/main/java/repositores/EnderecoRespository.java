package repositores;


import model.EnderecoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnderecoRespository extends JpaRepository<EnderecoModel, Integer  > {

}
