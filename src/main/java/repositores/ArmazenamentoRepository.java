package repositores;


import model.ArmazenamentoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArmazenamentoRepository extends JpaRepository<ArmazenamentoModel, Integer  > {

}
