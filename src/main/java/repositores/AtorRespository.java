package repositores;


import model.AtorModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AtorRespository extends JpaRepository<AtorModel, Integer  > {

}
