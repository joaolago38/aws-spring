package repositores;


import model.FilmeAtorModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmeActorRespository extends JpaRepository<FilmeAtorModel, Integer  > {

}
