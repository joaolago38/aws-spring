package repositores;


import model.PaisModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaisRepository extends JpaRepository<PaisModel, Integer  > {

}
