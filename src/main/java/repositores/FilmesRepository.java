package repositores;


import model.FilmeModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmesRepository extends JpaRepository<FilmeModel, Integer  > {

}
